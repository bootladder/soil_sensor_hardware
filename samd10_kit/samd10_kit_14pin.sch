<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="11" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Release" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Heatsink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="tMarkings" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="bMarkings" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="bBPL" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="MPL" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_layer121" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bplace" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="References" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="atsamd10c14a">
<packages>
<package name="SO014">
<wire x1="4.8768" y1="2.1463" x2="-3.6068" y2="2.1463" width="0.1524" layer="21"/>
<wire x1="4.8768" y1="-2.1463" x2="5.2578" y2="-1.7653" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.9878" y1="1.7653" x2="-3.6068" y2="2.1463" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.8768" y1="2.1463" x2="5.2578" y2="1.7653" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.9878" y1="-1.7653" x2="-3.6068" y2="-2.1463" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.6068" y1="-2.1463" x2="4.8768" y2="-2.1463" width="0.1524" layer="21"/>
<wire x1="5.2578" y1="-1.7653" x2="5.2578" y2="1.7653" width="0.1524" layer="21"/>
<wire x1="-3.9878" y1="1.7653" x2="-3.9878" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.9878" y1="1.27" x2="-3.9878" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.9878" y1="-1.27" x2="-3.9878" y2="-1.7653" width="0.1524" layer="21"/>
<wire x1="-3.9878" y1="1.27" x2="-3.9878" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<smd name="1" x="-3.175" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="2" x="-1.905" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="3" x="-0.635" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="4" x="0.635" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="5" x="1.905" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="6" x="3.175" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="7" x="4.445" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="8" x="4.445" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="9" x="3.175" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="10" x="1.905" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="11" x="0.635" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="12" x="-0.635" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="13" x="-1.905" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="14" x="-3.175" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<text x="-4.318" y="-2.6035" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-3.429" y1="-2.2733" x2="-2.921" y2="-2.1463" layer="21"/>
<rectangle x1="-3.429" y1="-2.2733" x2="-2.921" y2="-2.1463" layer="21"/>
<rectangle x1="-3.429" y1="-3.429" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-2.2733" x2="-1.651" y2="-2.1463" layer="21"/>
<rectangle x1="-2.159" y1="-3.429" x2="-1.651" y2="-1.9558" layer="51"/>
<rectangle x1="-0.889" y1="-2.2733" x2="-0.381" y2="-2.1463" layer="21"/>
<rectangle x1="-0.889" y1="-3.429" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="-3.429" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="-2.2733" x2="0.889" y2="-2.1463" layer="21"/>
<rectangle x1="1.651" y1="-2.2733" x2="2.159" y2="-2.1463" layer="21"/>
<rectangle x1="1.651" y1="-3.429" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-2.2733" x2="3.429" y2="-2.1463" layer="21"/>
<rectangle x1="2.921" y1="-3.429" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-2.2733" x2="4.699" y2="-2.1463" layer="21"/>
<rectangle x1="4.191" y1="-3.429" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="2.1463" x2="-2.921" y2="2.2733" layer="21"/>
<rectangle x1="-3.429" y1="2.1463" x2="-2.921" y2="2.2733" layer="21"/>
<rectangle x1="-3.429" y1="2.2733" x2="-2.921" y2="3.7465" layer="51"/>
<rectangle x1="-2.159" y1="2.1463" x2="-1.651" y2="2.2733" layer="21"/>
<rectangle x1="-2.159" y1="2.2733" x2="-1.651" y2="3.7465" layer="51"/>
<rectangle x1="-0.889" y1="2.1463" x2="-0.381" y2="2.2733" layer="21"/>
<rectangle x1="-0.889" y1="2.2733" x2="-0.381" y2="3.7465" layer="51"/>
<rectangle x1="0.381" y1="2.1463" x2="0.889" y2="2.2733" layer="21"/>
<rectangle x1="0.381" y1="2.2733" x2="0.889" y2="3.7465" layer="51"/>
<rectangle x1="1.651" y1="2.1463" x2="2.159" y2="2.2733" layer="21"/>
<rectangle x1="1.651" y1="2.2733" x2="2.159" y2="3.7465" layer="51"/>
<rectangle x1="2.921" y1="2.1463" x2="3.429" y2="2.2733" layer="21"/>
<rectangle x1="2.921" y1="2.2733" x2="3.429" y2="3.7465" layer="51"/>
<rectangle x1="4.191" y1="2.1463" x2="4.699" y2="2.2733" layer="21"/>
<rectangle x1="4.191" y1="2.2733" x2="4.699" y2="3.7465" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="ATSAMD10C14A">
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-17.78" x2="43.18" y2="-17.78" width="0.254" layer="94"/>
<wire x1="43.18" y1="-17.78" x2="43.18" y2="2.54" width="0.254" layer="94"/>
<wire x1="43.18" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<pin name="PA05" x="-7.62" y="0" length="middle"/>
<pin name="PA08" x="-7.62" y="-2.54" length="middle"/>
<pin name="PA09" x="-7.62" y="-5.08" length="middle"/>
<pin name="PA14" x="-7.62" y="-7.62" length="middle"/>
<pin name="PA15" x="-7.62" y="-10.16" length="middle"/>
<pin name="PA28_NRST" x="-7.62" y="-12.7" length="middle"/>
<pin name="PA30_SWCLK" x="-7.62" y="-15.24" length="middle"/>
<pin name="PA31_SWDIO" x="48.26" y="-15.24" length="middle" rot="R180"/>
<pin name="PA24" x="48.26" y="-12.7" length="middle" rot="R180"/>
<pin name="PA25" x="48.26" y="-10.16" length="middle" rot="R180"/>
<pin name="GND" x="48.26" y="-7.62" length="middle" rot="R180"/>
<pin name="VDD" x="48.26" y="-5.08" length="middle" rot="R180"/>
<pin name="PA02" x="48.26" y="-2.54" length="middle" rot="R180"/>
<pin name="PA04" x="48.26" y="0" length="middle" rot="R180"/>
<text x="-5.08" y="5.08" size="1.27" layer="94">&gt;NAME</text>
<text x="40.64" y="5.08" size="1.27" layer="94">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATSAMD10C14A">
<gates>
<gate name="G$1" symbol="ATSAMD10C14A" x="-2.54" y="2.54"/>
</gates>
<devices>
<device name="" package="SO014">
<connects>
<connect gate="G$1" pin="GND" pad="11"/>
<connect gate="G$1" pin="PA02" pad="13"/>
<connect gate="G$1" pin="PA04" pad="14"/>
<connect gate="G$1" pin="PA05" pad="1"/>
<connect gate="G$1" pin="PA08" pad="2"/>
<connect gate="G$1" pin="PA09" pad="3"/>
<connect gate="G$1" pin="PA14" pad="4"/>
<connect gate="G$1" pin="PA15" pad="5"/>
<connect gate="G$1" pin="PA24" pad="9"/>
<connect gate="G$1" pin="PA25" pad="10"/>
<connect gate="G$1" pin="PA28_NRST" pad="6"/>
<connect gate="G$1" pin="PA30_SWCLK" pad="7"/>
<connect gate="G$1" pin="PA31_SWDIO" pad="8"/>
<connect gate="G$1" pin="VDD" pad="12"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microbuilder">
<packages>
<package name="2X05_1.27MM_SMT">
<description>&lt;p&gt;2x05 1.27mm Headers SMT (2.0mm Height)&lt;/p&gt;
4UConnector: 16846</description>
<wire x1="-5.27" y1="3.5" x2="5.27" y2="3.5" width="0.2032" layer="21"/>
<wire x1="5.27" y1="3.5" x2="5.27" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="5.27" y1="-3.5" x2="1" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="1" y1="-3.5" x2="-1" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="-1" y1="-3.5" x2="-5.27" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="-5.27" y1="-3.5" x2="-5.27" y2="3.5" width="0.2032" layer="21"/>
<wire x1="-1" y1="-3.5" x2="-1" y2="-4" width="0.2032" layer="21"/>
<wire x1="-1" y1="-4" x2="1" y2="-4" width="0.2032" layer="21"/>
<wire x1="1" y1="-4" x2="1" y2="-3.5" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="-1.85" dx="0.65" dy="2.6" layer="1"/>
<smd name="2" x="-2.54" y="1.85" dx="0.65" dy="2.6" layer="1"/>
<smd name="3" x="-1.27" y="-1.85" dx="0.65" dy="2.6" layer="1"/>
<smd name="4" x="-1.27" y="1.85" dx="0.65" dy="2.6" layer="1"/>
<smd name="5" x="0" y="-1.85" dx="0.65" dy="2.6" layer="1"/>
<smd name="6" x="0" y="1.85" dx="0.65" dy="2.6" layer="1"/>
<smd name="7" x="1.27" y="-1.85" dx="0.65" dy="2.6" layer="1"/>
<smd name="8" x="1.27" y="1.85" dx="0.65" dy="2.6" layer="1"/>
<smd name="9" x="2.54" y="-1.85" dx="0.65" dy="2.6" layer="1"/>
<smd name="10" x="2.54" y="1.85" dx="0.65" dy="2.6" layer="1"/>
<text x="-5.199" y="3.806" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.199" y="-4.2465" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="21">
<vertex x="1.016" y="-3.556"/>
<vertex x="1.016" y="-3.937"/>
<vertex x="-1.016" y="-3.937"/>
<vertex x="-1.016" y="-3.556"/>
</polygon>
</package>
<package name="2X05_1.27MM_BOX_NOPOSTS">
<description>&lt;p&gt;4UCon: 19735 (with cap), 15117 (without caps)&lt;/p&gt;</description>
<wire x1="-6.45" y1="2.6" x2="6.45" y2="2.6" width="0.127" layer="51"/>
<wire x1="6.45" y1="2.6" x2="6.45" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-6.45" y1="-2.6" x2="-6.45" y2="2.6" width="0.127" layer="51"/>
<wire x1="-1.2" y1="-2.6" x2="-6.45" y2="-2.6" width="0.127" layer="51"/>
<wire x1="6.45" y1="-2.6" x2="1.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-5.575" y1="1.8" x2="5.575" y2="1.8" width="0.127" layer="51"/>
<wire x1="5.575" y1="1.8" x2="5.575" y2="-1.8" width="0.127" layer="51"/>
<wire x1="-5.575" y1="-1.8" x2="-5.575" y2="1.8" width="0.127" layer="51"/>
<wire x1="-1.2" y1="-1.8" x2="-5.575" y2="-1.8" width="0.127" layer="51"/>
<wire x1="5.575" y1="-1.8" x2="1.2" y2="-1.8" width="0.127" layer="51"/>
<wire x1="-1.2" y1="-1.8" x2="-1.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="1.2" y1="-1.8" x2="1.2" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-3.2" y1="2.7" x2="-6.6" y2="2.7" width="0.127" layer="21"/>
<wire x1="-6.6" y1="2.7" x2="-6.6" y2="-2.7" width="0.127" layer="21"/>
<wire x1="-6.6" y1="-2.7" x2="-3.2" y2="-2.7" width="0.127" layer="21"/>
<wire x1="3.2" y1="2.7" x2="6.6" y2="2.7" width="0.127" layer="21"/>
<wire x1="6.6" y1="2.7" x2="6.6" y2="-2.7" width="0.127" layer="21"/>
<wire x1="6.6" y1="-2.7" x2="3.2" y2="-2.7" width="0.127" layer="21"/>
<smd name="1" x="-2.54" y="-1.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="2" x="-2.54" y="1.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="3" x="-1.27" y="-1.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="4" x="-1.27" y="1.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="5" x="0" y="-1.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="6" x="0" y="1.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="7" x="1.27" y="-1.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="8" x="1.27" y="1.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="9" x="2.54" y="-1.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="10" x="2.54" y="1.95" dx="0.76" dy="2.4" layer="1"/>
<text x="-6.6" y="3" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-6.1" y="-2.4" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="21">
<vertex x="-6.5" y="-4"/>
<vertex x="-5.5" y="-4"/>
<vertex x="-6" y="-3"/>
</polygon>
</package>
<package name="2X05_1.27MM_BOX_POSTS">
<description>&lt;p&gt;4UCon: 20317&lt;/p&gt;</description>
<wire x1="-6.275" y1="2.5" x2="6.275" y2="2.5" width="0.127" layer="51"/>
<wire x1="6.275" y1="2.5" x2="6.275" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-6.275" y1="-2.5" x2="-6.275" y2="2.5" width="0.127" layer="51"/>
<wire x1="-1.25" y1="-2.5" x2="-6.275" y2="-2.5" width="0.127" layer="51"/>
<wire x1="6.275" y1="-2.5" x2="1.25" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-5.425" y1="1.65" x2="5.425" y2="1.65" width="0.127" layer="51"/>
<wire x1="5.425" y1="1.65" x2="5.425" y2="-1.65" width="0.127" layer="51"/>
<wire x1="-5.425" y1="-1.65" x2="-5.425" y2="1.65" width="0.127" layer="51"/>
<wire x1="-1.25" y1="-1.65" x2="-5.425" y2="-1.65" width="0.127" layer="51"/>
<wire x1="5.425" y1="-1.65" x2="1.25" y2="-1.65" width="0.127" layer="51"/>
<wire x1="-1.25" y1="-1.65" x2="-1.25" y2="-2.5" width="0.127" layer="51"/>
<wire x1="1.25" y1="-1.65" x2="1.25" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-3.2" y1="2.7" x2="-6.5" y2="2.7" width="0.127" layer="21"/>
<wire x1="-6.5" y1="2.7" x2="-6.5" y2="-2.7" width="0.127" layer="21"/>
<wire x1="-6.5" y1="-2.7" x2="-3.2" y2="-2.7" width="0.127" layer="21"/>
<wire x1="3.2" y1="2.7" x2="6.5" y2="2.7" width="0.127" layer="21"/>
<wire x1="6.5" y1="2.7" x2="6.5" y2="-2.7" width="0.127" layer="21"/>
<wire x1="6.5" y1="-2.7" x2="3.2" y2="-2.7" width="0.127" layer="21"/>
<smd name="1" x="-2.54" y="-2.15" dx="0.76" dy="2.6" layer="1"/>
<smd name="2" x="-2.54" y="2.15" dx="0.76" dy="2.6" layer="1"/>
<smd name="3" x="-1.27" y="-2.15" dx="0.76" dy="2.6" layer="1"/>
<smd name="4" x="-1.27" y="2.15" dx="0.76" dy="2.6" layer="1"/>
<smd name="5" x="0" y="-2.15" dx="0.76" dy="2.6" layer="1"/>
<smd name="6" x="0" y="2.15" dx="0.76" dy="2.6" layer="1"/>
<smd name="7" x="1.27" y="-2.15" dx="0.76" dy="2.6" layer="1"/>
<smd name="8" x="1.27" y="2.15" dx="0.76" dy="2.6" layer="1"/>
<smd name="9" x="2.54" y="-2.15" dx="0.76" dy="2.6" layer="1"/>
<smd name="10" x="2.54" y="2.15" dx="0.76" dy="2.6" layer="1"/>
<text x="-6.6" y="3" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-6.1" y="-2.4" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<hole x="-1.905" y="0" drill="1"/>
<hole x="1.905" y="0" drill="1"/>
<polygon width="0.127" layer="21">
<vertex x="-6.5" y="-4"/>
<vertex x="-5.5" y="-4"/>
<vertex x="-6" y="-3"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="JTAG-CORTEX">
<circle x="-4.445" y="5.08" radius="0.635" width="0.254" layer="94"/>
<circle x="-4.445" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-4.445" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="-4.445" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-4.445" y="-5.08" radius="0.635" width="0.254" layer="94"/>
<circle x="4.445" y="5.08" radius="0.635" width="0.254" layer="94"/>
<circle x="4.445" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="4.445" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="4.445" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="4.445" y="-5.08" radius="0.635" width="0.254" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-7.62" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<pin name="GND@1" x="-7.62" y="2.54" visible="off" length="short" direction="pwr" swaplevel="1"/>
<pin name="GND@2" x="-7.62" y="0" visible="off" length="short" direction="pwr" swaplevel="1"/>
<pin name="GNDDETECT" x="-7.62" y="-5.08" visible="off" length="short" swaplevel="1"/>
<pin name="KEY" x="-7.62" y="-2.54" visible="off" length="short" swaplevel="1"/>
<pin name="NRESET" x="7.62" y="-5.08" visible="off" length="short" swaplevel="1" rot="R180"/>
<pin name="SWDCLK" x="7.62" y="2.54" visible="off" length="short" swaplevel="1" rot="R180"/>
<pin name="SWDIO" x="7.62" y="5.08" visible="off" length="short" swaplevel="1" rot="R180"/>
<pin name="SWO" x="7.62" y="0" visible="off" length="short" swaplevel="1" rot="R180"/>
<pin name="TDI" x="7.62" y="-2.54" visible="off" length="short" swaplevel="1" rot="R180"/>
<pin name="VCC" x="-7.62" y="5.08" visible="off" length="short" direction="pwr" swaplevel="1"/>
<text x="-6.35" y="8.255" size="1.27" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.318" size="0.635" layer="95">nRESET</text>
<text x="1.778" y="-1.778" size="0.635" layer="95">NC/TDI</text>
<text x="1.016" y="0.762" size="0.635" layer="95">SWO/TDO</text>
<text x="-0.762" y="3.302" size="0.635" layer="95">SWDCLK/TCK</text>
<text x="0" y="5.842" size="0.635" layer="95">SWDIO/TMS</text>
<text x="-5.588" y="6.096" size="0.635" layer="95">VCC</text>
<text x="-5.588" y="3.302" size="0.635" layer="95">GND</text>
<text x="-5.588" y="0.762" size="0.635" layer="95">GND</text>
<text x="-5.588" y="-1.778" size="0.635" layer="95">KEY</text>
<text x="-5.588" y="-4.318" size="0.635" layer="95">GNDDetect</text>
<text x="-6.35" y="-9.525" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="JTAG-CORTEX" prefix="X" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;Serial Wire (SW-DP) Connector&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;Standard 0.05" 10-pin connector is for use with the Cortex M and Cortex A SWD (serial wire debugger) interface (SW-DP)&lt;/p&gt;
&lt;b&gt;Connectors&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;2x05_1.27MM_SMT&lt;/b&gt; - Inexpensive surface-mount header with bare pins, compatible with standard 10-pin SWD cables (4UCON: 16846)&lt;/li&gt;
&lt;li&gt;&lt;b&gt;2x05_1.27MM_BOX_NOPOSTS&lt;/b&gt; - Polarised surface-mount box (4UCON with caps: 19735, without caps: 15117&lt;/li&gt;
&lt;li&gt;&lt;b&gt;2x05_1.27MM_BOX_POSTS&lt;/b&gt; - Polarised surface-mount box with mounting holes (4UCON: 20317)&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;Pinout:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf&lt;/li&gt;
&lt;li&gt;http://www.keil.com/peripherals/coresight/connectors.asp&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="JTAG-CORTEX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X05_1.27MM_SMT">
<connects>
<connect gate="G$1" pin="GND@1" pad="3"/>
<connect gate="G$1" pin="GND@2" pad="5"/>
<connect gate="G$1" pin="GNDDETECT" pad="9"/>
<connect gate="G$1" pin="KEY" pad="7"/>
<connect gate="G$1" pin="NRESET" pad="10"/>
<connect gate="G$1" pin="SWDCLK" pad="4"/>
<connect gate="G$1" pin="SWDIO" pad="2"/>
<connect gate="G$1" pin="SWO" pad="6"/>
<connect gate="G$1" pin="TDI" pad="8"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BOX" package="2X05_1.27MM_BOX_NOPOSTS">
<connects>
<connect gate="G$1" pin="GND@1" pad="3"/>
<connect gate="G$1" pin="GND@2" pad="5"/>
<connect gate="G$1" pin="GNDDETECT" pad="9"/>
<connect gate="G$1" pin="KEY" pad="7"/>
<connect gate="G$1" pin="NRESET" pad="10"/>
<connect gate="G$1" pin="SWDCLK" pad="4"/>
<connect gate="G$1" pin="SWDIO" pad="2"/>
<connect gate="G$1" pin="SWO" pad="6"/>
<connect gate="G$1" pin="TDI" pad="8"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BOXPOSTS" package="2X05_1.27MM_BOX_POSTS">
<connects>
<connect gate="G$1" pin="GND@1" pad="3"/>
<connect gate="G$1" pin="GND@2" pad="5"/>
<connect gate="G$1" pin="GNDDETECT" pad="9"/>
<connect gate="G$1" pin="KEY" pad="7"/>
<connect gate="G$1" pin="NRESET" pad="10"/>
<connect gate="G$1" pin="SWDCLK" pad="4"/>
<connect gate="G$1" pin="SWDIO" pad="2"/>
<connect gate="G$1" pin="SWO" pad="6"/>
<connect gate="G$1" pin="TDI" pad="8"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Simple Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 0.1"</description>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-1.27" y2="1.27" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.254" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-3.81" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="square" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-3.8862" y="1.8288" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="1X03M">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2MM"</description>
<wire x1="3.25" y1="1.25" x2="3.25" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="3.25" y1="-1.25" x2="-1" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.25" x2="-3.25" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-3.25" y1="-1.25" x2="-3.25" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-3.25" y1="1.25" x2="-1" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="1.25" x2="3.25" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="1.25" x2="-1" y2="-1.25" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="0" drill="0.9144" shape="square"/>
<pad name="2" x="0" y="0" drill="0.9144"/>
<pad name="3" x="2" y="0" drill="0.9144" rot="R270"/>
<text x="-4" y="-1" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="5" y="-1" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<rectangle x1="-2.25" y1="-0.25" x2="-1.75" y2="0.25" layer="51"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51" rot="R270"/>
</package>
<package name="1X07">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 0.1"</description>
<wire x1="8.89" y1="-1.27" x2="-8.89" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-8.89" y1="1.27" x2="-8.89" y2="-1.27" width="0.254" layer="21"/>
<wire x1="8.89" y1="1.27" x2="8.89" y2="-1.27" width="0.254" layer="21"/>
<wire x1="8.89" y1="1.27" x2="-6.35" y2="1.27" width="0.254" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="-8.89" y2="1.27" width="0.254" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="-1.27" width="0.254" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="square" rot="R90"/>
<pad name="2" x="-5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="-2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="4" x="0" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="6" x="5.08" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="7" x="7.62" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-8.9662" y="1.8288" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-8.89" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-7.874" y1="-0.254" x2="-7.366" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
</package>
<package name="1X07M">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2MM"</description>
<wire x1="7.25" y1="1.25" x2="7.25" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="7.25" y1="-1.25" x2="-5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-5" y1="-1.25" x2="-7.25" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-7.25" y1="-1.25" x2="-7.25" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-7.25" y1="1.25" x2="-5" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-5" y1="1.25" x2="7.25" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-5" y1="1.25" x2="-5" y2="-1.25" width="0.2032" layer="21"/>
<pad name="1" x="-6" y="0" drill="0.9144" shape="square"/>
<pad name="2" x="-4" y="0" drill="0.9144"/>
<pad name="3" x="-2" y="0" drill="0.9144" rot="R270"/>
<pad name="4" x="0" y="0" drill="0.9144" rot="R270"/>
<pad name="5" x="2" y="0" drill="0.9144" rot="R270"/>
<pad name="6" x="4" y="0" drill="0.9144" rot="R270"/>
<pad name="7" x="6" y="0" drill="0.9144" rot="R270"/>
<text x="-8" y="-1" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="9" y="-1" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.25" y1="-0.25" x2="-3.75" y2="0.25" layer="51"/>
<rectangle x1="-6.25" y1="-0.25" x2="-5.75" y2="0.25" layer="51"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51" rot="R270"/>
<rectangle x1="-2.25" y1="-0.25" x2="-1.75" y2="0.25" layer="51" rot="R270"/>
<rectangle x1="3.75" y1="-0.25" x2="4.25" y2="0.25" layer="51" rot="R270"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51" rot="R270"/>
<rectangle x1="5.75" y1="-0.25" x2="6.25" y2="0.25" layer="51" rot="R270"/>
</package>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 0.1"</description>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="0" y2="1.27" width="0.254" layer="21"/>
<wire x1="0" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="square" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02M">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2MM"</description>
<wire x1="2.25" y1="1.25" x2="2.25" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="2.25" y1="-1.25" x2="0" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="0" y1="-1.25" x2="-2.25" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-2.25" y1="-1.25" x2="-2.25" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-2.25" y1="1.25" x2="0" y2="1.25" width="0.2032" layer="21"/>
<wire x1="0" y1="1.25" x2="2.25" y2="1.25" width="0.2032" layer="21"/>
<wire x1="0" y1="1.25" x2="0" y2="-1.25" width="0.2032" layer="21"/>
<pad name="1" x="-1" y="0" drill="0.9144" shape="square"/>
<pad name="2" x="1" y="0" drill="0.9144"/>
<text x="-3" y="-1" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="4" y="-1" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.75" y1="-0.25" x2="1.25" y2="0.25" layer="51"/>
<rectangle x1="-1.25" y1="-0.25" x2="-0.75" y2="0.25" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PINHD3">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD7">
<wire x1="-6.35" y1="-10.16" x2="1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="10.16" x2="-6.35" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="-10.16" width="0.4064" layer="94"/>
<text x="-6.35" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X3" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M" package="1X03M">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X7" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD7" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X07">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M" package="1X07M">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M" package="1X02M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Originally created by librarian@cadsoft.de&lt;p&gt;Modifications and additions by Bob Starr (rtzaudio@mindspring.com)&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="1.1113" y1="1.5875" x2="0" y2="1.5875" width="0.254" layer="94"/>
<wire x1="0" y1="1.5875" x2="-1.1113" y2="1.5875" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.5875" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0.7938" x2="-0.635" y2="0.7938" width="0.254" layer="94"/>
<wire x1="0.1587" y1="0" x2="-0.1588" y2="0" width="0.254" layer="94"/>
<text x="-2.2225" y="-2.54" size="1.778" layer="96" ratio="12">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="point" direction="sup" rot="R270"/>
</symbol>
<symbol name="VDD">
<wire x1="0" y1="-2.54" x2="0" y2="-0.9525" width="0.1524" layer="94"/>
<circle x="0" y="-0.0001" radius="0.8979" width="0.4064" layer="94"/>
<text x="-2.54" y="1.5875" size="1.778" layer="96" ratio="12">&gt;VALUE</text>
<pin name="VDD" x="0" y="-2.54" visible="off" length="point" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V">
<wire x1="0" y1="-2.54" x2="0" y2="-0.9525" width="0.1524" layer="94"/>
<circle x="0" y="-0.0001" radius="0.8979" width="0.4064" layer="94"/>
<text x="-2.54" y="1.5875" size="1.778" layer="96" ratio="12">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="point" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VDD" prefix="V">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VDD" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="V">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-OPL-ic">
<packages>
<package name="SOT-223">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;</description>
<wire x1="3.277" y1="1.778" x2="3.277" y2="-1.778" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-1.778" x2="-3.277" y2="-1.778" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-1.778" x2="-3.277" y2="1.778" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="1.778" x2="3.277" y2="1.778" width="0.2032" layer="21"/>
<wire x1="-3.473" y1="4.483" x2="3.473" y2="4.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-4.483" x2="-3.473" y2="-4.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-4.483" x2="-3.473" y2="4.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="4.483" x2="3.473" y2="-4.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.286" y="-3.175" dx="1.27" dy="2.286" layer="1"/>
<smd name="2" x="0" y="-3.175" dx="1.27" dy="2.286" layer="1"/>
<smd name="3" x="2.286" y="-3.175" dx="1.27" dy="2.286" layer="1"/>
<smd name="TER" x="0" y="3.175" dx="3.556" dy="2.159" layer="1"/>
<text x="-3.81" y="-3.81" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="0.889" layer="27" ratio="11" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="PMIC-CJ*1117">
<text x="-7.62" y="3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="0" y="3.81" size="1.27" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="-7.62" length="short" rot="R90"/>
<pin name="IN" x="-11.43" y="0" length="short"/>
<pin name="OUT" x="11.43" y="0" length="short" rot="R180"/>
<wire x1="-7.62" y1="3.81" x2="7.62" y2="3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="3.81" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.81" x2="-7.62" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-8.89" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0" x2="8.89" y2="0" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PMIC-CJT1117-3.3(SOT223)" prefix="U">
<description>310030097</description>
<gates>
<gate name="G$1" symbol="PMIC-CJ*1117" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-223">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="2 TER"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="CJT1117-3.3"/>
<attribute name="VALUE" value="CJT1117-3.3-SOT223" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rc-master-smd">
<description>&lt;b&gt;R/C MASTER-SMD! - v1.14 (03/30/2013)&lt;/b&gt;&lt;p&gt;
&lt;p&gt;This library is a collection of SMD ONLY resistors and capacitors by various manufacturers. The pad sizes, spacing and silkscreen widths have been tweaked for use in dense fine pitch layouts where space, alignment and precision are critical. In general these components are designed for routing in grid increments of 5 mils&lt;/p&gt;
&lt;p&gt;* Silkscreen line elements are a minimum of 8 mils in width. All components have text sizes of 0.032"  or 0.04".&lt;/p&gt;
&lt;p&gt;* A silkscreen text values use a ratio of 18 in all cases.&lt;/p&gt;
&lt;p&gt;* ALL PADS AND PART OUTLINES ARE SNAPPED TO A 5 MIL GRID!&lt;/p&gt;
&lt;p&gt;* See ULP script &lt;b&gt;migrate-rc-master.ulp&lt;/b&gt; to auto-migrate parts from &lt;b&gt;rcl.lbr&lt;/b&gt;&lt;/h4&gt;.&lt;/p&gt;
&lt;p&gt;&lt;h4&gt;All components are prefixed using the following conventions:&lt;/h4&gt;&lt;/p&gt;&lt;br&gt;
&lt;table width="380" border="1" bordercolor="#000000"&gt;
  &lt;tr&gt; 
    &lt;td width="81" bgcolor="#33CCFF"&gt;&lt;div align="center"&gt;&lt;strong&gt;Prefix&lt;/strong&gt;&lt;/div&gt;&lt;/td&gt;
    &lt;td width="289" bgcolor="#33CCFF"&gt;&lt;div align="center"&gt;&lt;strong&gt;Description&lt;/strong&gt;&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CBP_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Bipolar Electrolytic Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CCA_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Chip Cap Array Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CP_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Polarized Electrolytic/Tantalum Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;C_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Non-Polarized Film / Chip Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;FB_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Simple Ferrite Bead Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;L_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Simple Chip Inductors&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;R_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Resistor Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
&lt;/table&gt;
&lt;p&gt;
As a general guideline, SMD resistors are typically rated as follows:&lt;p&gt;
0402 = 1/16 watt&lt;br&gt;
0603 = 1/10 watt&lt;br&gt;
0805 = 1/8  watt&lt;br&gt;
1206 = 1/4  watt&lt;br&gt;
2010 = 1/2  watt&lt;br&gt;
2512 = 1    watt&lt;br&gt;&lt;p&gt;
&lt;author&gt;THIS LIBRARY IS PROVIDED AS IS AND WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED.&lt;br&gt;Copyright (C) 2007-2008, Bob Starr&lt;br&gt; 
&lt;a href="http://www.bobstarr.net"&gt;http://www.bobstarr.net&lt;/a&gt;
&lt;/author&gt;</description>
<packages>
<package name="C1812">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1812</description>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="-3.175" y1="2.032" x2="3.175" y2="2.032" width="0.2032" layer="21"/>
<wire x1="3.175" y1="2.032" x2="3.175" y2="-2.032" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-2.032" x2="-3.175" y2="-2.032" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-2.032" x2="-3.175" y2="2.032" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.905" dy="3.4036" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.905" dy="3.4036" layer="1"/>
<text x="-2.8575" y="2.3813" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-3.3338" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3175" y1="-0.7" x2="0.3175" y2="0.7" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1825</description>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="-3.175" y1="3.683" x2="3.175" y2="3.683" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.683" x2="3.175" y2="-3.683" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-3.683" x2="-3.175" y2="-3.683" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-3.683" x2="-3.175" y2="3.683" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-2.8575" y="4.1275" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-4.7625" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAP&lt;/b&gt; - 2012</description>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="-1.905" y1="1.016" x2="1.905" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.016" x2="1.905" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.016" x2="-1.905" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-1.016" x2="-1.905" y2="1.016" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.0917" y1="-0.7239" x2="-0.3416" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C2220">
<description>&lt;b&gt;CAP&lt;/b&gt; - 2220 (5650)</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<wire x1="-3.81" y1="3.048" x2="3.81" y2="3.048" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-3.048" x2="-3.81" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-3.048" x2="-3.81" y2="3.048" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-3.4925" y="3.4925" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-4.445" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2225">
<description>&lt;b&gt;CAP&lt;/b&gt; - 2225 (5664)</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<wire x1="-3.81" y1="3.683" x2="3.81" y2="3.683" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.683" x2="3.81" y2="-3.683" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-3.683" x2="-3.81" y2="-3.683" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-3.683" x2="-3.81" y2="3.683" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-3.4925" y="4.1275" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-5.08" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAP&lt;/b&gt; - 3216</description>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-2.794" y1="1.27" x2="2.794" y2="1.27" width="0.2032" layer="21"/>
<wire x1="2.794" y1="1.27" x2="2.794" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="2.794" y1="-1.27" x2="-2.794" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-2.794" y1="-1.27" x2="-2.794" y2="1.27" width="0.2032" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7013" y1="-0.8509" x2="-0.9512" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAP&lt;/b&gt; - 3225</description>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="-2.54" y1="1.651" x2="2.54" y2="1.651" width="0.2032" layer="21"/>
<wire x1="2.54" y1="1.651" x2="2.54" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.651" x2="-2.54" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-1.651" x2="-2.54" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-1.397" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.397" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.2225" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.6988" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7013" y1="-1.2954" x2="-0.9512" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.304" x2="1.7018" y2="1.2959" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAP&lt;/b&gt; - 4532</description>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="-3.175" y1="2.032" x2="3.175" y2="2.032" width="0.2032" layer="21"/>
<wire x1="3.175" y1="2.032" x2="3.175" y2="-2.032" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-2.032" x2="-3.175" y2="-2.032" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-2.032" x2="-3.175" y2="2.032" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-2.8575" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAP&lt;/b&gt; - 4564</description>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="-3.175" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-3.81" x2="-3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-3.81" x2="-3.175" y2="3.81" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-2.8575" y="4.1275" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-5.08" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C0402">
<description>&lt;b&gt;CAP&lt;/b&gt; - 0402</description>
<wire x1="-0.245" y1="0.174" x2="0.245" y2="0.174" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.174" x2="-0.245" y2="-0.174" width="0.1016" layer="51"/>
<wire x1="-1.0573" y1="0.5557" x2="1.0573" y2="0.5557" width="0.2032" layer="21"/>
<wire x1="1.0573" y1="0.5557" x2="1.0573" y2="-0.5556" width="0.2032" layer="21"/>
<wire x1="1.0573" y1="-0.5556" x2="-1.0573" y2="-0.5557" width="0.2032" layer="21"/>
<wire x1="-1.0573" y1="-0.5557" x2="-1.0573" y2="0.5557" width="0.2032" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.6" dy="0.6" layer="1"/>
<smd name="2" x="0.508" y="0" dx="0.6" dy="0.6" layer="1"/>
<text x="-0.9525" y="0.7939" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.9525" y="-1.5876" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.0794" y1="-0.2381" x2="0.0794" y2="0.2381" layer="35"/>
<rectangle x1="0.25" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
<rectangle x1="-0.5" y1="-0.25" x2="-0.25" y2="0.25" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.0004" layer="57"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.0004" layer="57"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.0004" layer="57"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.0004" layer="57"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAP&lt;/b&gt; - 0603</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="-1.4605" y1="0.635" x2="1.4605" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.4605" y1="0.635" x2="1.4605" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.4605" y1="-0.635" x2="-1.4605" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.4605" y1="-0.635" x2="-1.4605" y2="0.635" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.9" dy="0.8" layer="1"/>
<smd name="2" x="0.762" y="0" dx="0.9" dy="0.8" layer="1"/>
<text x="-1.27" y="0.9525" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.7463" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8382" y2="0.4" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.85" y1="0.4" x2="0.85" y2="0.4" width="0.0006" layer="57"/>
<wire x1="0.85" y1="0.4" x2="0.85" y2="-0.4" width="0.0006" layer="57"/>
<wire x1="0.85" y1="-0.4" x2="-0.85" y2="-0.4" width="0.0006" layer="57"/>
<wire x1="-0.85" y1="-0.4" x2="-0.85" y2="0.4" width="0.0006" layer="57"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAP&lt;/b&gt; - 0805</description>
<wire x1="-0.41" y1="0.585" x2="0.41" y2="0.585" width="0.1016" layer="51"/>
<wire x1="-0.41" y1="-0.585" x2="0.41" y2="-0.585" width="0.1016" layer="51"/>
<wire x1="-1.905" y1="0.889" x2="1.905" y2="0.889" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0.889" x2="1.905" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-0.889" x2="-1.905" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-0.889" x2="-1.905" y2="0.889" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<text x="-1.5875" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.5874" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1.0564" y2="0.65" layer="51"/>
<rectangle x1="-1.0668" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1.05" y1="0.65" x2="1.05" y2="0.65" width="0.0006" layer="57"/>
<wire x1="1.05" y1="0.65" x2="1.05" y2="-0.65" width="0.0006" layer="57"/>
<wire x1="1.05" y1="-0.65" x2="-1.05" y2="-0.65" width="0.0006" layer="57"/>
<wire x1="-1.05" y1="-0.65" x2="-1.05" y2="0.65" width="0.0006" layer="57"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1206</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1016" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1016" layer="51"/>
<wire x1="-2.286" y1="1.143" x2="2.286" y2="1.143" width="0.2032" layer="21"/>
<wire x1="2.286" y1="1.143" x2="2.286" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="2.286" y1="-1.143" x2="-2.286" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="-2.286" y1="-1.143" x2="-2.286" y2="1.143" width="0.2032" layer="21"/>
<smd name="2" x="1.27" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="1" x="-1.27" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-2.0638" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.0638" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<wire x1="-1.65" y1="0.85" x2="1.65" y2="0.85" width="0.0006" layer="57"/>
<wire x1="1.65" y1="0.85" x2="1.65" y2="-0.85" width="0.0006" layer="57"/>
<wire x1="1.65" y1="-0.85" x2="-1.65" y2="-0.85" width="0.0006" layer="57"/>
<wire x1="-1.65" y1="-0.85" x2="-1.65" y2="0.85" width="0.0006" layer="57"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1210</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1016" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1016" layer="51"/>
<wire x1="-2.794" y1="1.651" x2="2.794" y2="1.651" width="0.2032" layer="21"/>
<wire x1="2.794" y1="1.651" x2="2.794" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="2.794" y1="-1.651" x2="-2.794" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-2.794" y1="-1.651" x2="-2.794" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="C0201">
<description>&lt;b&gt;CAP&lt;/b&gt; - 0201</description>
<wire x1="-0.195" y1="0.124" x2="0.195" y2="0.124" width="0.1016" layer="51"/>
<wire x1="0.195" y1="-0.124" x2="-0.195" y2="-0.124" width="0.1016" layer="51"/>
<wire x1="-0.7938" y1="0.4763" x2="0.7938" y2="0.4763" width="0.2032" layer="21"/>
<wire x1="0.7938" y1="0.4763" x2="0.7938" y2="-0.4763" width="0.2032" layer="21"/>
<wire x1="0.7938" y1="-0.4763" x2="-0.7938" y2="-0.4763" width="0.2032" layer="21"/>
<wire x1="-0.7938" y1="-0.4763" x2="-0.7938" y2="0.4763" width="0.2032" layer="21"/>
<smd name="1" x="-0.3175" y="0" dx="0.5" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="0.3175" y="0" dx="0.5" dy="0.4" layer="1" rot="R90"/>
<text x="-0.7938" y="0.7939" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.7938" y="-1.5876" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.304" y1="-0.2" x2="-0.15" y2="0.2" layer="51"/>
<rectangle x1="0.15" y1="-0.2" x2="0.3088" y2="0.2" layer="51"/>
<wire x1="-0.3" y1="0.2" x2="0.3" y2="0.2" width="0.0006" layer="57"/>
<wire x1="0.3" y1="0.2" x2="0.3" y2="-0.2" width="0.0006" layer="57"/>
<wire x1="0.3" y1="-0.2" x2="-0.3" y2="-0.2" width="0.0006" layer="57"/>
<wire x1="-0.3" y1="-0.2" x2="-0.3" y2="0.2" width="0.0006" layer="57"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1608</description>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="1.524" y1="0.762" x2="1.524" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.524" y1="-0.762" x2="-1.524" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.524" y1="-0.762" x2="-1.524" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.524" y1="0.762" x2="1.524" y2="0.762" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="1" dy="1" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1" dy="1" layer="1"/>
<text x="-1.27" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1808">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1808</description>
<wire x1="-1.4732" y1="1.0002" x2="1.4732" y2="1.0002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.0002" x2="1.4732" y2="-1.0002" width="0.1016" layer="51"/>
<wire x1="-3.429" y1="1.778" x2="3.429" y2="1.778" width="0.2032" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.429" y2="-1.778" width="0.2032" layer="21"/>
<wire x1="3.429" y1="-1.778" x2="-3.429" y2="-1.778" width="0.2032" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="-3.429" y2="1.778" width="0.2032" layer="21"/>
<smd name="1" x="-2.159" y="0" dx="1.8" dy="2.9" layer="1"/>
<smd name="2" x="2.159" y="0" dx="1.8" dy="2.9" layer="1"/>
<text x="-3.175" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.1" x2="-1.4376" y2="1.1" layer="51"/>
<rectangle x1="1.4478" y1="-1.1" x2="2.3978" y2="1.1" layer="51"/>
</package>
<package name="C0201MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 0201 MicroPitch</description>
<wire x1="-0.195" y1="0.124" x2="0.195" y2="0.124" width="0.1016" layer="51"/>
<wire x1="0.195" y1="-0.124" x2="-0.195" y2="-0.124" width="0.1016" layer="51"/>
<smd name="1" x="-0.254" y="0" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="0.254" y="0" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<text x="-0.3175" y="0.3175" size="0.6096" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.3175" y="-0.635" size="0.3048" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.304" y1="-0.2" x2="-0.15" y2="0.2" layer="51"/>
<rectangle x1="0.15" y1="-0.2" x2="0.3088" y2="0.2" layer="51"/>
<wire x1="-0.3" y1="0.2" x2="0.3" y2="0.2" width="0.0006" layer="57"/>
<wire x1="0.3" y1="0.2" x2="0.3" y2="-0.2" width="0.0006" layer="57"/>
<wire x1="0.3" y1="-0.2" x2="-0.3" y2="-0.2" width="0.0006" layer="57"/>
<wire x1="-0.3" y1="-0.2" x2="-0.3" y2="0.2" width="0.0006" layer="57"/>
</package>
<package name="C0402MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 0402 MicroPitch&lt;p&gt;</description>
<wire x1="-0.245" y1="0.174" x2="0.245" y2="0.174" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.174" x2="-0.245" y2="-0.174" width="0.1016" layer="51"/>
<wire x1="0" y1="0.127" x2="0" y2="-0.127" width="0.2032" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<smd name="2" x="0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<text x="-0.635" y="0.4763" size="0.6096" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.635" y="-0.7938" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.2" x2="0.1" y2="0.2" layer="35"/>
<rectangle x1="-0.5" y1="-0.25" x2="-0.254" y2="0.25" layer="51"/>
<rectangle x1="0.2588" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.0004" layer="57"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.0004" layer="57"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.0004" layer="57"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.0004" layer="57"/>
</package>
<package name="C0603MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 0603 MicroPitch</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="0" y1="0.254" x2="0" y2="-0.254" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="2" x="0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.9525" y="0.635" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.9525" y="-0.9525" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<rectangle x1="-0.8" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.25" x2="0.1999" y2="0.25" layer="35"/>
<wire x1="-0.85" y1="0.4" x2="0.85" y2="0.4" width="0.0006" layer="57"/>
<wire x1="0.85" y1="0.4" x2="0.85" y2="-0.4" width="0.0006" layer="57"/>
<wire x1="0.85" y1="-0.4" x2="-0.85" y2="-0.4" width="0.0006" layer="57"/>
<wire x1="-0.85" y1="-0.4" x2="-0.85" y2="0.4" width="0.0006" layer="57"/>
</package>
<package name="C0805MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 0805 MicroPitch</description>
<wire x1="-0.51" y1="0.535" x2="0.51" y2="0.535" width="0.1016" layer="51"/>
<wire x1="-0.51" y1="-0.535" x2="0.51" y2="-0.535" width="0.1016" layer="51"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<text x="-1.5875" y="0.9525" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.5875" y="-1.27" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1" y2="0.65" layer="51"/>
<rectangle x1="-1" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.0004" layer="57"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.0004" layer="57"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.0004" layer="57"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.0004" layer="57"/>
</package>
<package name="C1206MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1206 MicroPitch&lt;p&gt;</description>
<wire x1="1.0525" y1="-0.7128" x2="-1.0652" y2="-0.7128" width="0.1016" layer="51"/>
<wire x1="1.0525" y1="0.7128" x2="-1.0652" y2="0.7128" width="0.1016" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="0.635" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-0.635" x2="0.635" y2="-0.635" width="0.2032" layer="21"/>
<smd name="2" x="1.524" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="1" x="-1.524" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-2.2225" y="1.1113" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-1.4288" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-0.9" y2="0.8" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<rectangle x1="0.9001" y1="-0.8" x2="1.6" y2="0.8" layer="51" rot="R180"/>
<wire x1="-1.6" y1="0.8" x2="1.6" y2="0.8" width="0.0006" layer="57"/>
<wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.0006" layer="57"/>
<wire x1="1.6" y1="-0.8" x2="-1.6" y2="-0.8" width="0.0006" layer="57"/>
<wire x1="-1.6" y1="-0.8" x2="-1.6" y2="0.8" width="0.0006" layer="57"/>
</package>
<package name="C1210MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1210 MicroPitch&lt;p&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1016" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1016" layer="51"/>
<wire x1="-0.508" y1="1.143" x2="0.508" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="-1.143" x2="0.508" y2="-1.143" width="0.2032" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="C1608MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1608 MicroPitch&lt;p&gt;</description>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.381" x2="0" y2="-0.381" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="1" dy="1" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1" dy="1" layer="1"/>
<text x="-1.27" y="0.9525" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1808MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1808 MicroPitch&lt;p&gt;</description>
<wire x1="-1.4732" y1="1.0002" x2="1.4732" y2="1.0002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.0002" x2="1.4732" y2="-1.0002" width="0.1016" layer="51"/>
<wire x1="-0.889" y1="1.27" x2="0.889" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="0.889" y2="-1.27" width="0.2032" layer="21"/>
<smd name="1" x="-2.159" y="0" dx="1.8" dy="2.9" layer="1"/>
<smd name="2" x="2.159" y="0" dx="1.8" dy="2.9" layer="1"/>
<text x="-3.175" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.1" x2="-1.4376" y2="1.1" layer="51"/>
<rectangle x1="1.4478" y1="-1.1" x2="2.3978" y2="1.1" layer="51"/>
</package>
<package name="C1812MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1812 MicroPitch&lt;p&gt;</description>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="-0.635" y1="1.524" x2="0.635" y2="1.524" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.524" x2="0.635" y2="-1.524" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.905" dy="3.4036" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.905" dy="3.4036" layer="1"/>
<text x="-2.8575" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3175" y1="-0.7" x2="0.3175" y2="0.7" layer="35"/>
</package>
<package name="C1825MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1825 MicroPitch&lt;p&gt;</description>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="-0.635" y1="3.175" x2="0.635" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-3.175" x2="0.635" y2="-3.175" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-2.8575" y="3.81" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-4.445" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 2012 MicroPitch&lt;p&gt;</description>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="-0.127" y1="0.635" x2="0.127" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-0.127" y1="-0.635" x2="0.127" y2="-0.635" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.5875" y="0.9525" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.5875" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.0917" y1="-0.7239" x2="-0.3416" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C2220MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 2220 (5650)  MicroPitch&lt;p&gt;</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<wire x1="-1.524" y1="2.667" x2="1.524" y2="2.667" width="0.2032" layer="21"/>
<wire x1="-1.524" y1="-2.667" x2="1.524" y2="-2.667" width="0.2032" layer="21"/>
<smd name="1" x="-2.794" y="0" dx="1.85" dy="5.588" layer="1"/>
<smd name="2" x="2.794" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-3.4925" y="3.175" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-3.81" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2225MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 2225 (5664) MicroPitch&lt;p&gt;</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<wire x1="-1.27" y1="3.302" x2="1.27" y2="3.302" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-3.302" x2="1.27" y2="-3.302" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-3.4925" y="3.81" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-4.7625" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C3216MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 3216 MicroPitch&lt;p&gt;</description>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-0.508" y1="0.762" x2="0.508" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="-0.762" x2="0.508" y2="-0.762" width="0.2032" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-2.2225" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7013" y1="-0.8509" x2="-0.9512" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 3225 MicroPitch&lt;p&gt;</description>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="0.254" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.254" y1="-1.27" x2="0.254" y2="-1.27" width="0.2032" layer="21"/>
<smd name="1" x="-1.397" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.397" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7013" y1="-1.2954" x2="-0.9512" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.304" x2="1.7018" y2="1.2959" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 4532 MicroPitch&lt;p&gt;</description>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="-0.635" y1="1.524" x2="0.635" y2="1.524" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.524" x2="0.635" y2="-1.524" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-2.8575" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 4564 MicroPitch&lt;p&gt;</description>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="-0.635" y1="3.175" x2="0.635" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-3.175" x2="0.635" y2="-3.175" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-2.8575" y="3.81" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-4.7625" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C2824">
<description>&lt;b&gt;CAP&lt;/b&gt; - 2824</description>
<wire x1="-2.6732" y1="3.0002" x2="2.6732" y2="3.0002" width="0.1016" layer="51"/>
<wire x1="-2.6478" y1="-3.0002" x2="2.6732" y2="-3.0002" width="0.1016" layer="51"/>
<wire x1="-5.207" y1="3.683" x2="5.207" y2="3.683" width="0.2032" layer="21"/>
<wire x1="5.207" y1="3.683" x2="5.207" y2="-3.683" width="0.2032" layer="21"/>
<wire x1="5.207" y1="-3.683" x2="-5.207" y2="-3.683" width="0.2032" layer="21"/>
<wire x1="-5.207" y1="-3.683" x2="-5.207" y2="3.683" width="0.2032" layer="21"/>
<smd name="1" x="-3.556" y="0" dx="2.54" dy="6.604" layer="1"/>
<smd name="2" x="3.529" y="0" dx="2.54" dy="6.604" layer="1"/>
<text x="-5.08" y="4.1275" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.08" y="-4.7625" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.5876" y1="-3.05" x2="-2.6376" y2="3.05" layer="51"/>
<rectangle x1="2.6478" y1="-3.05" x2="3.5978" y2="3.05" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2824MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 2824 MicroPitch&lt;p&gt;</description>
<wire x1="-1.905" y1="3.175" x2="1.905" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-3.175" x2="-1.905" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="-2.6732" y1="3.0002" x2="2.6732" y2="3.0002" width="0.1016" layer="51"/>
<wire x1="-2.6478" y1="-3.0002" x2="2.6732" y2="-3.0002" width="0.1016" layer="51"/>
<smd name="1" x="-3.556" y="0" dx="2.286" dy="6.35" layer="1"/>
<smd name="2" x="3.429" y="0" dx="2.286" dy="6.35" layer="1"/>
<text x="-4.7625" y="3.4925" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.7625" y="-4.445" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.5876" y1="-3.05" x2="-2.6376" y2="3.05" layer="51"/>
<rectangle x1="2.6478" y1="-3.05" x2="3.5978" y2="3.05" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C5040">
<description>&lt;b&gt;CAP&lt;/b&gt; - 5040</description>
<wire x1="-9.017" y1="5.842" x2="9.017" y2="5.842" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="5" x2="5.4" y2="5" width="0.1016" layer="51"/>
<wire x1="-5.4" y1="-5" x2="5.4" y2="-5" width="0.1016" layer="51"/>
<wire x1="-9.017" y1="-5.842" x2="9.017" y2="-5.842" width="0.2032" layer="21"/>
<wire x1="-9.017" y1="-5.842" x2="-9.017" y2="5.842" width="0.2032" layer="21"/>
<wire x1="9.017" y1="-5.842" x2="9.017" y2="5.842" width="0.2032" layer="21"/>
<smd name="1" x="-6.35" y="0" dx="4.445" dy="10.922" layer="1"/>
<smd name="2" x="6.35" y="0" dx="4.445" dy="10.922" layer="1"/>
<text x="-8.89" y="6.35" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-8.89" y="-6.985" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.3" y1="-5.1" x2="-5.3" y2="5.1" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
<rectangle x1="5.3" y1="-5.1" x2="6.3" y2="5.1" layer="51"/>
</package>
<package name="C5040MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 5040 MicroPitch</description>
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="5" x2="5.4" y2="5" width="0.1016" layer="51"/>
<wire x1="-5.4" y1="-5" x2="5.4" y2="-5" width="0.1016" layer="51"/>
<wire x1="-3.81" y1="-5.08" x2="3.81" y2="-5.08" width="0.2032" layer="21"/>
<smd name="1" x="-6.35" y="0" dx="4.191" dy="10.795" layer="1"/>
<smd name="2" x="6.35" y="0" dx="4.191" dy="10.668" layer="1"/>
<text x="-8.255" y="6.0325" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-8.255" y="-6.6675" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.3" y1="-5.1" x2="-5.3" y2="5.1" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
<rectangle x1="5.3" y1="-5.1" x2="6.3" y2="5.1" layer="51"/>
</package>
<package name="C6054">
<description>&lt;b&gt;CAP&lt;/b&gt; - 6054</description>
<wire x1="-10.668" y1="7.239" x2="10.668" y2="7.239" width="0.2032" layer="21"/>
<wire x1="-6.7" y1="6.8" x2="6.7" y2="6.8" width="0.1016" layer="51"/>
<wire x1="-6.7" y1="-6.8" x2="6.7" y2="-6.8" width="0.1016" layer="51"/>
<wire x1="-10.668" y1="-7.239" x2="10.668" y2="-7.239" width="0.2032" layer="21"/>
<wire x1="-10.668" y1="-7.239" x2="-10.668" y2="7.239" width="0.2032" layer="21"/>
<wire x1="10.668" y1="-7.239" x2="10.668" y2="7.239" width="0.2032" layer="21"/>
<smd name="1" x="-7.62" y="0" dx="5.08" dy="13.1064" layer="1"/>
<smd name="2" x="7.62" y="0" dx="5.08" dy="13.1064" layer="1"/>
<text x="-10.16" y="7.62" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-10.16" y="-8.255" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.6" y1="-6.9" x2="-6.6" y2="6.9" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
<rectangle x1="6.6" y1="-6.9" x2="7.6" y2="6.9" layer="51"/>
</package>
<package name="C6054MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 6054 MicroPitch</description>
<wire x1="-5.08" y1="6.985" x2="5.08" y2="6.985" width="0.2032" layer="21"/>
<wire x1="-6.7" y1="6.8" x2="6.7" y2="6.8" width="0.1016" layer="51"/>
<wire x1="-6.7" y1="-6.8" x2="6.7" y2="-6.8" width="0.1016" layer="51"/>
<wire x1="-5.08" y1="-6.985" x2="5.08" y2="-6.985" width="0.2032" layer="21"/>
<smd name="1" x="-7.62" y="0" dx="4.699" dy="13.1064" layer="1"/>
<smd name="2" x="7.62" y="0" dx="4.699" dy="13.1064" layer="1"/>
<text x="-9.525" y="7.62" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-9.525" y="-8.255" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.6" y1="-6.9" x2="-6.6" y2="6.9" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
<rectangle x1="6.6" y1="-6.9" x2="7.6" y2="6.9" layer="51"/>
</package>
<package name="C1913">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1913 (4833)</description>
<wire x1="-1.7" y1="1.3" x2="1.7" y2="1.3" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-1.3" x2="1.7" y2="-1.3" width="0.1016" layer="51"/>
<wire x1="-3.302" y1="1.905" x2="3.302" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.302" y1="1.905" x2="3.302" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.302" y1="-1.905" x2="-3.302" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-3.302" y1="-1.905" x2="-3.302" y2="1.905" width="0.2032" layer="21"/>
<smd name="1" x="-2.159" y="0" dx="1.6" dy="3.2" layer="1"/>
<smd name="2" x="2.159" y="0" dx="1.6" dy="3.2" layer="1"/>
<text x="-2.54" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3417" y1="-1.4" x2="-1.5916" y2="1.4" layer="51"/>
<rectangle x1="1.6056" y1="-1.4" x2="2.3557" y2="1.4" layer="51"/>
<rectangle x1="-0.3175" y1="-0.3175" x2="0.3175" y2="0.3175" layer="35"/>
</package>
<package name="C1913MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1913 (4833) MicroPitch</description>
<wire x1="-1.7" y1="1.3" x2="1.7" y2="1.3" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-1.3" x2="1.7" y2="-1.3" width="0.1016" layer="51"/>
<wire x1="-1.4" y1="1.3" x2="1.4" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="-1.3" x2="1.4" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-2.286" y="0" dx="1.3" dy="3" layer="1"/>
<smd name="2" x="2.286" y="0" dx="1.3" dy="3" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3417" y1="-1.4" x2="-1.5916" y2="1.4" layer="51"/>
<rectangle x1="1.6056" y1="-1.4" x2="2.3557" y2="1.4" layer="51"/>
<rectangle x1="-0.3175" y1="-0.3175" x2="0.3175" y2="0.3175" layer="35"/>
</package>
<package name="C2416">
<description>&lt;b&gt;CAP&lt;/b&gt; - 2416 (6041)</description>
<wire x1="-3.937" y1="2.413" x2="3.937" y2="2.413" width="0.2032" layer="21"/>
<wire x1="3.937" y1="2.413" x2="3.937" y2="-2.413" width="0.2032" layer="21"/>
<wire x1="3.937" y1="-2.413" x2="-3.937" y2="-2.413" width="0.2032" layer="21"/>
<wire x1="-3.937" y1="-2.413" x2="-3.937" y2="2.413" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="2" x2="2.3" y2="2" width="0.1016" layer="51"/>
<wire x1="-2.4" y1="-2" x2="2.3" y2="-2" width="0.1016" layer="51"/>
<smd name="1" x="-2.667" y="0" dx="2" dy="4.3" layer="1"/>
<smd name="2" x="2.667" y="0" dx="2" dy="4.3" layer="1"/>
<text x="-3.81" y="2.667" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-3.556" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.0417" y1="-2.032" x2="-2.2916" y2="2.032" layer="51"/>
<rectangle x1="2.2056" y1="-2.032" x2="2.9557" y2="2.032" layer="51"/>
<rectangle x1="-0.3175" y1="-0.3175" x2="0.3175" y2="0.3175" layer="35"/>
</package>
<package name="C2416MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 2416 (6041) MicroPitch</description>
<wire x1="-2" y1="1.7" x2="2" y2="1.7" width="0.1016" layer="51"/>
<wire x1="-2" y1="-1.7" x2="2" y2="-1.7" width="0.1016" layer="51"/>
<wire x1="-1.6" y1="1.8" x2="1.6" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.8" x2="1.6" y2="-1.8" width="0.2032" layer="21"/>
<smd name="1" x="-2.667" y="0" dx="1.5" dy="3.8" layer="1"/>
<smd name="2" x="2.667" y="0" dx="1.5" dy="3.8" layer="1"/>
<text x="-2.8575" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.6417" y1="-1.8" x2="-1.8916" y2="1.8" layer="51"/>
<rectangle x1="1.9056" y1="-1.8" x2="2.6557" y2="1.8" layer="51"/>
<rectangle x1="-0.3175" y1="-0.3175" x2="0.3175" y2="0.3175" layer="35"/>
</package>
<package name="C2211">
<description>&lt;b&gt;CAP&lt;/b&gt; - 2211</description>
<wire x1="2.3" y1="-1.4" x2="-2.3" y2="-1.4" width="0.1016" layer="51"/>
<wire x1="-3.81" y1="2.032" x2="3.81" y2="2.032" width="0.2032" layer="21"/>
<wire x1="3.81" y1="2.032" x2="3.81" y2="-2.032" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-2.032" x2="-3.81" y2="-2.032" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-2.032" x2="-3.81" y2="2.032" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.4" x2="-2.3" y2="1.4" width="0.1016" layer="51"/>
<smd name="1" x="-2.54" y="0" dx="1.85" dy="3.5" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.85" dy="3.5" layer="1"/>
<text x="-3.81" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-1.5" x2="-2.2" y2="1.5" layer="51"/>
<rectangle x1="2.2" y1="-1.5" x2="2.8" y2="1.5" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2211MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 2211 MicroPitch</description>
<wire x1="2.3" y1="-1.4" x2="-2.3" y2="-1.4" width="0.1016" layer="51"/>
<wire x1="2.3" y1="1.4" x2="-2.3" y2="1.4" width="0.1016" layer="51"/>
<wire x1="-1.3" y1="1.4" x2="1.3" y2="1.4" width="0.2032" layer="21"/>
<wire x1="-1.3" y1="-1.4" x2="1.3" y2="-1.4" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="0" dx="1.85" dy="3" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.85" dy="3" layer="1"/>
<text x="-3.4925" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-1.5" x2="-2.2" y2="1.5" layer="51"/>
<rectangle x1="2.2" y1="-1.5" x2="2.8" y2="1.5" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C1111">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1111</description>
<wire x1="0.7525" y1="-1.4128" x2="-0.7652" y2="-1.4128" width="0.1016" layer="51"/>
<wire x1="0.7525" y1="1.4128" x2="-0.7652" y2="1.4128" width="0.1016" layer="51"/>
<wire x1="-2.286" y1="1.905" x2="2.286" y2="1.905" width="0.2032" layer="21" curve="-1.189668"/>
<wire x1="2.286" y1="1.905" x2="2.286" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="2.286" y1="-1.905" x2="-2.286" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-2.286" y1="-1.905" x2="-2.286" y2="1.905" width="0.2032" layer="21"/>
<smd name="2" x="1.27" y="0" dx="1.4" dy="3.2" layer="1"/>
<smd name="1" x="-1.27" y="0" dx="1.4" dy="3.2" layer="1"/>
<text x="-2.2225" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-3.0163" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3891" y1="-1.5" x2="-0.6525" y2="1.5" layer="51"/>
<rectangle x1="0.6525" y1="-1.5" x2="1.3891" y2="1.5" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<wire x1="-1.4" y1="1.5" x2="1.4" y2="1.5" width="0.0004" layer="57"/>
<wire x1="1.4" y1="1.5" x2="1.4" y2="-1.5" width="0.0004" layer="57"/>
<wire x1="1.4" y1="-1.5" x2="-1.4" y2="-1.5" width="0.0004" layer="57"/>
<wire x1="-1.4" y1="-1.5" x2="-1.4" y2="1.5" width="0.0004" layer="57"/>
</package>
<package name="C1111MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 1111 MicroPitch</description>
<wire x1="0.7525" y1="-1.4128" x2="-0.7652" y2="-1.4128" width="0.1016" layer="51"/>
<wire x1="0.7525" y1="1.4128" x2="-0.7652" y2="1.4128" width="0.1016" layer="51"/>
<smd name="2" x="1.27" y="0" dx="1.4" dy="3" layer="1"/>
<smd name="1" x="-1.27" y="0" dx="1.4" dy="3" layer="1"/>
<text x="-1.905" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3891" y1="-1.5" x2="-0.6525" y2="1.5" layer="51"/>
<rectangle x1="0.6525" y1="-1.5" x2="1.3891" y2="1.5" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<wire x1="-1.4" y1="1.5" x2="1.4" y2="1.5" width="0.0004" layer="57"/>
<wire x1="1.4" y1="1.5" x2="1.4" y2="-1.5" width="0.0004" layer="57"/>
<wire x1="1.4" y1="-1.5" x2="-1.4" y2="-1.5" width="0.0004" layer="57"/>
<wire x1="-1.4" y1="-1.5" x2="-1.4" y2="1.5" width="0.0004" layer="57"/>
</package>
<package name="C7361">
<description>&lt;b&gt;CAP&lt;/b&gt; - 7361</description>
<wire x1="-4.826" y1="3.302" x2="4.826" y2="3.302" width="0.2032" layer="21"/>
<wire x1="4.826" y1="3.302" x2="4.826" y2="-3.302" width="0.2032" layer="21"/>
<wire x1="4.826" y1="-3.302" x2="-4.826" y2="-3.302" width="0.2032" layer="21"/>
<wire x1="-4.826" y1="-3.302" x2="-4.826" y2="3.302" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="3.05" x2="3.65" y2="3.05" width="0.0508" layer="51"/>
<wire x1="3.65" y1="3.05" x2="3.65" y2="-3.05" width="0.0508" layer="51"/>
<wire x1="3.65" y1="-3.05" x2="-3.65" y2="-3.05" width="0.0508" layer="51"/>
<wire x1="-3.65" y1="-3.05" x2="-3.65" y2="3.05" width="0.0508" layer="51"/>
<smd name="2" x="3.175" y="0" dx="2.6" dy="3.2" layer="1"/>
<smd name="1" x="-3.175" y="0" dx="2.6" dy="3.2" layer="1"/>
<text x="-4.445" y="3.4925" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.445" y="-4.1275" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C7361MP">
<description>&lt;b&gt;CAP&lt;/b&gt; - 7361</description>
<wire x1="-3.937" y1="3.302" x2="3.937" y2="3.302" width="0.2032" layer="21"/>
<wire x1="3.937" y1="3.302" x2="3.937" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="3.937" y2="-3.302" width="0.2032" layer="21"/>
<wire x1="3.937" y1="-3.302" x2="-3.937" y2="-3.302" width="0.2032" layer="21"/>
<wire x1="-3.937" y1="-3.302" x2="-3.937" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-3.937" y2="3.302" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="3.05" x2="3.65" y2="3.05" width="0.0508" layer="51"/>
<wire x1="3.65" y1="3.05" x2="3.65" y2="-3.05" width="0.0508" layer="51"/>
<wire x1="3.65" y1="-3.05" x2="-3.65" y2="-3.05" width="0.0508" layer="51"/>
<wire x1="-3.65" y1="-3.05" x2="-3.65" y2="3.05" width="0.0508" layer="51"/>
<smd name="2" x="3.175" y="0" dx="2.6" dy="3.2" layer="1"/>
<smd name="1" x="-3.175" y="0" dx="2.6" dy="3.2" layer="1"/>
<text x="-3.81" y="3.4925" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CNP-">
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.635" y2="0" width="0.1524" layer="94"/>
<text x="1.905" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.175" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.1113" y1="-1.5875" x2="-0.4763" y2="1.5875" layer="94"/>
<rectangle x1="0.4763" y1="-1.5875" x2="1.1113" y2="1.5875" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C_" prefix="C" uservalue="yes">
<description>&lt;b&gt;NON-POLARIZED CAP&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="CNP-" x="2.54" y="0"/>
</gates>
<devices>
<device name="1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="C2220">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2225" package="C2225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201MP" package="C0201MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402MP" package="C0402MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603MP" package="C0603MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805MP" package="C0805MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206MP" package="C1206MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210MP" package="C1210MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1608MP" package="C1608MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1808MP" package="C1808MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812MP" package="C1812MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825MP" package="C1825MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012MP" package="C2012MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220MP" package="C2220MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2225MP" package="C2225MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216MP" package="C3216MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3225MP" package="C3225MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4532MP" package="C4532MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4564MP" package="C4564MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2824" package="C2824">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2824MP" package="C2824MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5040" package="C5040">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5040MP" package="C5040MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6054" package="C6054">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6054MP" package="C6054MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1913" package="C1913">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1913MP" package="C1913MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2416" package="C2416">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2416MP" package="C2416MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2211" package="C2211">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2211MP" package="C2211MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1111" package="C1111">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1111MP" package="C1111MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7361" package="C7361">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7361MP" package="C7361MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="atsamd10c14a" deviceset="ATSAMD10C14A" device=""/>
<part name="X1" library="microbuilder" deviceset="JTAG-CORTEX" device=""/>
<part name="JP1" library="pinhead" deviceset="PINHD-1X3" device=""/>
<part name="JP2" library="pinhead" deviceset="PINHD-1X3" device=""/>
<part name="GND1" library="supply" deviceset="GND" device=""/>
<part name="V1" library="supply" deviceset="VDD" device=""/>
<part name="GND2" library="supply" deviceset="GND" device=""/>
<part name="V2" library="supply" deviceset="VDD" device=""/>
<part name="GND3" library="supply" deviceset="GND" device=""/>
<part name="JP3" library="pinhead" deviceset="PINHD-1X7" device=""/>
<part name="JP4" library="pinhead" deviceset="PINHD-1X2" device=""/>
<part name="V3" library="supply" deviceset="VDD" device=""/>
<part name="GND4" library="supply" deviceset="GND" device=""/>
<part name="U1" library="Seeed-OPL-ic" deviceset="PMIC-CJT1117-3.3(SOT223)" device="" value="CJT1117-3.3-SOT223"/>
<part name="JP5" library="pinhead" deviceset="PINHD-1X2" device=""/>
<part name="JP6" library="pinhead" deviceset="PINHD-1X2" device=""/>
<part name="GND5" library="supply" deviceset="GND" device=""/>
<part name="C1" library="rc-master-smd" deviceset="C_" device="0603"/>
<part name="C3" library="rc-master-smd" deviceset="C_" device="0603"/>
<part name="C4" library="rc-master-smd" deviceset="C_" device="0603"/>
<part name="C5" library="rc-master-smd" deviceset="C_" device="0603"/>
<part name="V4" library="supply" deviceset="+5V" device=""/>
<part name="V5" library="supply" deviceset="VDD" device=""/>
<part name="GND6" library="supply" deviceset="GND" device=""/>
<part name="GND7" library="supply" deviceset="GND" device=""/>
<part name="C2" library="rc-master-smd" deviceset="C_" device="0603"/>
<part name="C6" library="rc-master-smd" deviceset="C_" device="0603"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="73.66" y="55.88"/>
<instance part="X1" gate="G$1" x="93.98" y="20.32"/>
<instance part="JP1" gate="A" x="139.7" y="63.5"/>
<instance part="JP2" gate="A" x="149.86" y="63.5"/>
<instance part="GND1" gate="1" x="139.7" y="45.72"/>
<instance part="V1" gate="G$1" x="147.32" y="50.8"/>
<instance part="GND2" gate="1" x="78.74" y="15.24"/>
<instance part="V2" gate="G$1" x="73.66" y="25.4"/>
<instance part="GND3" gate="1" x="121.92" y="68.58" rot="R270"/>
<instance part="JP3" gate="A" x="10.16" y="48.26" rot="R180"/>
<instance part="JP4" gate="G$1" x="175.26" y="45.72"/>
<instance part="V3" gate="G$1" x="200.66" y="40.64"/>
<instance part="GND4" gate="1" x="160.02" y="40.64"/>
<instance part="U1" gate="G$1" x="182.88" y="25.4"/>
<instance part="JP5" gate="G$1" x="185.42" y="66.04"/>
<instance part="JP6" gate="G$1" x="195.58" y="66.04"/>
<instance part="GND5" gate="1" x="187.96" y="60.96"/>
<instance part="C1" gate="G$1" x="190.5" y="48.26" rot="R90"/>
<instance part="C3" gate="G$1" x="200.66" y="48.26" rot="R90"/>
<instance part="C4" gate="G$1" x="205.74" y="48.26" rot="R90"/>
<instance part="C5" gate="G$1" x="210.82" y="48.26" rot="R90"/>
<instance part="V4" gate="1" x="180.34" y="53.34"/>
<instance part="V5" gate="G$1" x="215.9" y="55.88"/>
<instance part="GND6" gate="1" x="215.9" y="40.64"/>
<instance part="GND7" gate="1" x="187.96" y="38.1"/>
<instance part="C2" gate="G$1" x="195.58" y="48.26" rot="R90"/>
<instance part="C6" gate="G$1" x="215.9" y="48.26" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<wire x1="58.42" y1="30.48" x2="58.42" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="PA30_SWCLK"/>
<wire x1="58.42" y1="40.64" x2="66.04" y2="40.64" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="SWDCLK"/>
<wire x1="101.6" y1="22.86" x2="93.98" y2="22.86" width="0.1524" layer="91"/>
<wire x1="93.98" y1="22.86" x2="93.98" y2="30.48" width="0.1524" layer="91"/>
<wire x1="93.98" y1="30.48" x2="58.42" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="127" y1="25.4" x2="127" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="PA31_SWDIO"/>
<wire x1="127" y1="40.64" x2="121.92" y2="40.64" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="SWDIO"/>
<wire x1="101.6" y1="25.4" x2="127" y2="25.4" width="0.1524" layer="91"/>
<wire x1="127" y1="25.4" x2="127" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="NRESET"/>
<wire x1="101.6" y1="15.24" x2="106.68" y2="15.24" width="0.1524" layer="91"/>
<wire x1="106.68" y1="15.24" x2="106.68" y2="10.16" width="0.1524" layer="91"/>
<wire x1="106.68" y1="10.16" x2="53.34" y2="10.16" width="0.1524" layer="91"/>
<wire x1="53.34" y1="10.16" x2="53.34" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="PA28_NRST"/>
<wire x1="53.34" y1="43.18" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA05"/>
<wire x1="66.04" y1="55.88" x2="58.42" y2="55.88" width="0.1524" layer="91"/>
<wire x1="58.42" y1="55.88" x2="58.42" y2="63.5" width="0.1524" layer="91"/>
<wire x1="58.42" y1="63.5" x2="137.16" y2="63.5" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="2"/>
<pinref part="JP2" gate="A" pin="2"/>
<wire x1="137.16" y1="63.5" x2="147.32" y2="63.5" width="0.1524" layer="91"/>
<junction x="137.16" y="63.5"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA04"/>
<wire x1="121.92" y1="55.88" x2="127" y2="55.88" width="0.1524" layer="91"/>
<wire x1="127" y1="55.88" x2="127" y2="60.96" width="0.1524" layer="91"/>
<wire x1="127" y1="60.96" x2="137.16" y2="60.96" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="3"/>
<pinref part="JP2" gate="A" pin="3"/>
<wire x1="137.16" y1="60.96" x2="147.32" y2="60.96" width="0.1524" layer="91"/>
<junction x="137.16" y="60.96"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA14"/>
<pinref part="JP3" gate="A" pin="4"/>
<wire x1="66.04" y1="48.26" x2="12.7" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA15"/>
<pinref part="JP3" gate="A" pin="3"/>
<wire x1="66.04" y1="45.72" x2="12.7" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA02"/>
<wire x1="121.92" y1="53.34" x2="157.48" y2="53.34" width="0.1524" layer="91"/>
<wire x1="157.48" y1="53.34" x2="157.48" y2="73.66" width="0.1524" layer="91"/>
<wire x1="157.48" y1="73.66" x2="45.72" y2="73.66" width="0.1524" layer="91"/>
<wire x1="45.72" y1="73.66" x2="45.72" y2="55.88" width="0.1524" layer="91"/>
<pinref part="JP3" gate="A" pin="7"/>
<wire x1="45.72" y1="55.88" x2="12.7" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="121.92" y1="48.26" x2="139.7" y2="48.26" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="78.74" y1="17.78" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GND@2"/>
<wire x1="86.36" y1="20.32" x2="81.28" y2="20.32" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GND@1"/>
<wire x1="86.36" y1="22.86" x2="81.28" y2="22.86" width="0.1524" layer="91"/>
<wire x1="81.28" y1="22.86" x2="81.28" y2="20.32" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GNDDETECT"/>
<wire x1="86.36" y1="15.24" x2="81.28" y2="15.24" width="0.1524" layer="91"/>
<wire x1="81.28" y1="15.24" x2="81.28" y2="20.32" width="0.1524" layer="91"/>
<junction x="81.28" y="20.32"/>
<wire x1="78.74" y1="20.32" x2="81.28" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="124.46" y1="68.58" x2="127" y2="68.58" width="0.1524" layer="91"/>
<wire x1="127" y1="68.58" x2="127" y2="66.04" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="1"/>
<wire x1="127" y1="66.04" x2="137.16" y2="66.04" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="1"/>
<wire x1="137.16" y1="66.04" x2="147.32" y2="66.04" width="0.1524" layer="91"/>
<junction x="137.16" y="66.04"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="JP4" gate="G$1" pin="1"/>
<wire x1="172.72" y1="48.26" x2="160.02" y2="48.26" width="0.1524" layer="91"/>
<wire x1="160.02" y1="48.26" x2="160.02" y2="45.72" width="0.1524" layer="91"/>
<wire x1="160.02" y1="45.72" x2="160.02" y2="43.18" width="0.1524" layer="91"/>
<wire x1="172.72" y1="12.7" x2="172.72" y2="10.16" width="0.1524" layer="91"/>
<wire x1="172.72" y1="10.16" x2="154.94" y2="10.16" width="0.1524" layer="91"/>
<wire x1="154.94" y1="10.16" x2="154.94" y2="45.72" width="0.1524" layer="91"/>
<wire x1="154.94" y1="45.72" x2="160.02" y2="45.72" width="0.1524" layer="91"/>
<junction x="160.02" y="45.72"/>
<wire x1="172.72" y1="12.7" x2="180.34" y2="12.7" width="0.1524" layer="91"/>
<wire x1="182.88" y1="17.78" x2="180.34" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="JP5" gate="G$1" pin="2"/>
<pinref part="JP5" gate="G$1" pin="1"/>
<wire x1="182.88" y1="66.04" x2="182.88" y2="68.58" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="182.88" y1="68.58" x2="187.96" y2="68.58" width="0.1524" layer="91"/>
<wire x1="187.96" y1="68.58" x2="187.96" y2="63.5" width="0.1524" layer="91"/>
<junction x="182.88" y="68.58"/>
<pinref part="JP6" gate="G$1" pin="1"/>
<wire x1="187.96" y1="68.58" x2="190.5" y2="68.58" width="0.1524" layer="91"/>
<junction x="187.96" y="68.58"/>
<wire x1="190.5" y1="68.58" x2="193.04" y2="68.58" width="0.1524" layer="91"/>
<wire x1="190.5" y1="68.58" x2="190.5" y2="66.04" width="0.1524" layer="91"/>
<junction x="190.5" y="68.58"/>
<pinref part="JP6" gate="G$1" pin="2"/>
<wire x1="190.5" y1="66.04" x2="193.04" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="210.82" y1="45.72" x2="208.28" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="208.28" y1="45.72" x2="205.74" y2="45.72" width="0.1524" layer="91"/>
<wire x1="205.74" y1="45.72" x2="200.66" y2="45.72" width="0.1524" layer="91"/>
<junction x="205.74" y="45.72"/>
<wire x1="208.28" y1="45.72" x2="208.28" y2="43.18" width="0.1524" layer="91"/>
<junction x="208.28" y="45.72"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="208.28" y1="43.18" x2="215.9" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="210.82" y1="45.72" x2="215.9" y2="45.72" width="0.1524" layer="91"/>
<junction x="210.82" y="45.72"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="187.96" y1="40.64" x2="187.96" y2="45.72" width="0.1524" layer="91"/>
<wire x1="190.5" y1="45.72" x2="193.04" y2="45.72" width="0.1524" layer="91"/>
<wire x1="190.5" y1="45.72" x2="187.96" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<junction x="190.5" y="45.72"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="190.5" y1="45.72" x2="195.58" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="VDD"/>
<wire x1="121.92" y1="50.8" x2="142.24" y2="50.8" width="0.1524" layer="91"/>
<wire x1="142.24" y1="50.8" x2="142.24" y2="48.26" width="0.1524" layer="91"/>
<pinref part="V1" gate="G$1" pin="VDD"/>
<wire x1="142.24" y1="48.26" x2="147.32" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="VCC"/>
<wire x1="86.36" y1="25.4" x2="78.74" y2="25.4" width="0.1524" layer="91"/>
<wire x1="78.74" y1="25.4" x2="78.74" y2="22.86" width="0.1524" layer="91"/>
<pinref part="V2" gate="G$1" pin="VDD"/>
<wire x1="78.74" y1="22.86" x2="73.66" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V3" gate="G$1" pin="VDD"/>
<wire x1="180.34" y1="45.72" x2="200.66" y2="38.1" width="0.1524" layer="91"/>
<wire x1="193.04" y1="25.4" x2="194.31" y2="25.4" width="0.1524" layer="91"/>
<wire x1="194.31" y1="25.4" x2="195.58" y2="25.4" width="0.1524" layer="91"/>
<wire x1="195.58" y1="25.4" x2="195.58" y2="35.56" width="0.1524" layer="91"/>
<wire x1="195.58" y1="35.56" x2="195.58" y2="40.64" width="0.1524" layer="91"/>
<wire x1="195.58" y1="35.56" x2="200.66" y2="35.56" width="0.1524" layer="91"/>
<wire x1="200.66" y1="35.56" x2="200.66" y2="38.1" width="0.1524" layer="91"/>
<junction x="195.58" y="35.56"/>
<junction x="200.66" y="38.1"/>
<pinref part="U1" gate="G$1" pin="OUT"/>
<junction x="194.31" y="25.4"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="200.66" y1="50.8" x2="205.74" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="205.74" y1="50.8" x2="210.82" y2="50.8" width="0.1524" layer="91"/>
<junction x="205.74" y="50.8"/>
<wire x1="210.82" y1="50.8" x2="215.9" y2="50.8" width="0.1524" layer="91"/>
<junction x="210.82" y="50.8"/>
<pinref part="V5" gate="G$1" pin="VDD"/>
<wire x1="215.9" y1="50.8" x2="215.9" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<junction x="215.9" y="50.8"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA25"/>
<wire x1="121.92" y1="45.72" x2="134.62" y2="45.72" width="0.1524" layer="91"/>
<wire x1="134.62" y1="45.72" x2="134.62" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-10.16" x2="27.94" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-10.16" x2="27.94" y2="40.64" width="0.1524" layer="91"/>
<pinref part="JP3" gate="A" pin="1"/>
<wire x1="27.94" y1="40.64" x2="12.7" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA24"/>
<wire x1="121.92" y1="43.18" x2="132.08" y2="43.18" width="0.1524" layer="91"/>
<wire x1="132.08" y1="43.18" x2="132.08" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-7.62" x2="30.48" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-7.62" x2="30.48" y2="43.18" width="0.1524" layer="91"/>
<pinref part="JP3" gate="A" pin="2"/>
<wire x1="30.48" y1="43.18" x2="12.7" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA08"/>
<pinref part="JP3" gate="A" pin="6"/>
<wire x1="66.04" y1="53.34" x2="12.7" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA09"/>
<pinref part="JP3" gate="A" pin="5"/>
<wire x1="66.04" y1="50.8" x2="12.7" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="160.02" y1="20.32" x2="162.56" y2="20.32" width="0.1524" layer="91"/>
<wire x1="162.56" y1="20.32" x2="162.56" y2="25.4" width="0.1524" layer="91"/>
<wire x1="162.56" y1="25.4" x2="162.56" y2="33.02" width="0.1524" layer="91"/>
<wire x1="162.56" y1="33.02" x2="160.02" y2="33.02" width="0.1524" layer="91"/>
<wire x1="160.02" y1="33.02" x2="160.02" y2="35.56" width="0.1524" layer="91"/>
<wire x1="160.02" y1="35.56" x2="172.72" y2="35.56" width="0.1524" layer="91"/>
<pinref part="JP4" gate="G$1" pin="2"/>
<wire x1="172.72" y1="35.56" x2="182.88" y2="35.56" width="0.1524" layer="91"/>
<wire x1="172.72" y1="45.72" x2="172.72" y2="40.64" width="0.1524" layer="91"/>
<junction x="172.72" y="35.56"/>
<wire x1="172.72" y1="40.64" x2="172.72" y2="35.56" width="0.1524" layer="91"/>
<wire x1="167.64" y1="25.4" x2="162.56" y2="25.4" width="0.1524" layer="91"/>
<junction x="162.56" y="25.4"/>
<wire x1="171.45" y1="25.4" x2="162.56" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="IN"/>
<wire x1="172.72" y1="40.64" x2="180.34" y2="40.64" width="0.1524" layer="91"/>
<junction x="172.72" y="40.64"/>
<pinref part="V4" gate="1" pin="+5V"/>
<wire x1="180.34" y1="40.64" x2="180.34" y2="48.26" width="0.1524" layer="91"/>
<wire x1="180.34" y1="48.26" x2="180.34" y2="50.8" width="0.1524" layer="91"/>
<wire x1="180.34" y1="48.26" x2="185.42" y2="48.26" width="0.1524" layer="91"/>
<wire x1="185.42" y1="48.26" x2="185.42" y2="50.8" width="0.1524" layer="91"/>
<junction x="180.34" y="48.26"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="185.42" y1="50.8" x2="190.5" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="190.5" y1="50.8" x2="195.58" y2="50.8" width="0.1524" layer="91"/>
<junction x="190.5" y="50.8"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
